<?php declare(strict_types=1);

use App\Infrustructure\Service\ComplexNumberService;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$expression = '(3+4i)/(2+2i)';
echo '<pre>' . $expression . '</pre>';

$service = new ComplexNumberService($expression);

$resultComplexNumber = $service->compute();
echo 'Result: <br>';
echo 'Sting: ' . $resultComplexNumber() . '<br>';
echo 'JSON: ' . $resultComplexNumber->toJson() . '<br><br>';

echo 'ТЕСТЫ: '. '<br>';
$expressions = [
    '(3+4i)+(2+2i)' => '(5+6i)',
    '(3+4i)-(2+2i)' => '(1+2i)',
    '(3+4i)*(2+2i)' => '(-2+14i)',
    '(3+4i)/(2+2i)' => '(1.75+0.25i)',

    '(4i)+(2+2i)' => '(2+6i)',
    '(4i)-(2+2i)' => '(-2+2i)',
    '(4i)*(2+2i)' => '(-8+8i)',
    '(4i)/(2+2i)' => '(1+i)',

    '(5)+(2+2i)' => '(7+2i)',
    '(5)-(2+2i)' => '(3-2i)',
    '(5)*(2+2i)' => '(10+10i)',
    '(5)/(2+2i)' => '(1.25-1.25i)',

    '(i)+(2+2i)' => '(2+3i)',
    '(i)-(2+2i)' => '(-2-i)',
    '(i)*(2+2i)' => '(-2+2i)',
    '(i)/(2+2i)' => '(0.25+0.25i)',
];

function test(array $expressions) {
    foreach ($expressions as $expression => $standard) {
        $service = new ComplexNumberService($expression);
        $resultComplexNumber = $service->compute();
        $result = $resultComplexNumber() === $standard ? 'TRUE' : 'FALSE';

        echo $resultComplexNumber() . '=' . $standard . ' -> ' . $result . '<br>';
    }
}

test($expressions);