<?php declare(strict_types=1);

return (new PhpCsFixer\Config())
    ->setFinder(
        PhpCsFixer\Finder::create()
                         ->in(__DIR__ . '/src')
    )
    ->registerCustomFixers(new PhpCsFixerCustomFixers\Fixers())
    ->setRiskyAllowed(true)
    ->setRules([
        '@Symfony'                     => true,
        '@PhpCsFixer'                  => true,
        '@PhpCsFixer:risky'            => true,
        '@PHPUnit100Migration:risky'   => true,
        'declare_strict_types'         => true,
        'linebreak_after_opening_tag'  => false,
        'blank_line_after_opening_tag' => false,
        'phpdoc_param_order' => false,

        PhpCsFixerCustomFixers\Fixer\PhpdocSingleLineVarFixer::name()                  => true,
        PhpCsFixerCustomFixers\Fixer\SingleSpaceAfterStatementFixer::name()            => true,
        PhpCsFixerCustomFixers\Fixer\SingleSpaceBeforeStatementFixer::name()           => true,
        PhpCsFixerCustomFixers\Fixer\StringableInterfaceFixer::name()                  => true,
    ])
;
