<?php declare(strict_types=1);

namespace App\Application\Factory;

use App\Application\Exception\ValidationException;
use App\Application\Validator\ComplexNumberValidator;
use App\Domain\Entity\ComplexNumber;
use App\Domain\Factory\ComplexNumberFactoryInterface;

class ComplexNumberFactory implements ComplexNumberFactoryInterface
{
    /**
     * @throws ValidationException
     */
    public function create(string $complexStr): ComplexNumber
    {
        // Убираем типичные опечатки
        $complexStr = str_replace(['++', '--'], ['+', '-'], $complexStr);
        $typeNumber = ComplexNumberValidator::defineType($complexStr);

        return match ($typeNumber) {
            1 => $this->createFromComplexNumber($complexStr),
            2 => $this->createFromImaginaryNumber($complexStr),
            3 => $this->createFromRealNumber($complexStr),
            4 => $this->createFromSuffix(),
            default => throw new ValidationException('Невалидная строковая запись: '.$complexStr),
        };
    }

    /**
     * Создает комплексное число из стандартной его записи вида (a+bi).
     */
    private function createFromComplexNumber(string $complexStr): ComplexNumber
    {
        preg_match(ComplexNumberValidator::NUMBER_SPLIT_REGEXP, $complexStr, $complexParts);

        return new ComplexNumber((float) $complexParts[1], (float) $complexParts[4], $complexParts[9]);
    }

    /**
     * Создает из мнимой части комплексного числа (bi) число вида (0+bi).
     */
    private function createFromImaginaryNumber(string $complexStr): ComplexNumber
    {
        preg_match(ComplexNumberValidator::NUMBER_SPLIT_REGEXP, $complexStr, $complexParts);

        return new ComplexNumber(0, (float) $complexParts[1], $complexParts[9]);
    }

    /**
     * Создает из реального числа (b) комплексное вида (b+0i).
     */
    private function createFromRealNumber(string $complexStr): ComplexNumber
    {
        return new ComplexNumber((float) $complexStr);
    }

    /**
     * Создает из суффикса (i) комплексное число вида (0+1i).
     */
    private function createFromSuffix(): ComplexNumber
    {
        return new ComplexNumber(0, 1);
    }
}
