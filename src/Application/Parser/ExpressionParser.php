<?php declare(strict_types=1);

namespace App\Application\Parser;

use App\Application\Exception\ValidationException;
use App\Domain\Parser\ParserInterface;

final class ExpressionParser implements ParserInterface
{
    /**
     * @throws ValidationException
     */
    public function getFirstComplex(string $expression): string
    {
        $numbers = $this->extractComplexNumbers($expression);

        return $numbers[0];
    }

    /**
     * @throws ValidationException
     */
    public function getSecondComplex(string $expression): string
    {
        $numbers = $this->extractComplexNumbers($expression);

        return $numbers[1];
    }

    /**
     * @throws ValidationException
     */
    public function getOperation(string $expression): string
    {
        return $this->extractOperator($expression);
    }

    /**
     * @return string[] - первое комплексное число из строки и второе
     *
     * @throws ValidationException
     */
    private function extractComplexNumbers(string $expression): array
    {
        preg_match('/\((.*?)\)([+\-*\/])\((.*?)\)/', $expression, $matches);

        if (!\is_array($matches) || 4 !== \count($matches)) {
            throw new ValidationException('Невалидная запись одного или всех комплексных чисел');
        }

        return [trim($matches[1]), trim($matches[3])];
    }

    /**
     * @return string - знак операции между двумя комплексными числами, найденный в строковом выражении
     *
     * @throws ValidationException
     */
    private function extractOperator(string $expression): string
    {
        preg_match('/\((.*?)\)([+\-*\/])\((.*?)\)/', $expression, $matches);

        if (!\is_array($matches) || 4 !== \count($matches)) {
            throw new ValidationException('Невалидная запись одного или всех комплексных чисел');
        }

        return trim($matches[2]);
    }
}
