<?php declare(strict_types=1);

namespace App\Application\Validator;

class ComplexNumberValidator
{
    public const INVALID_TYPE = 0;

    public const COMPLEX_NUMBER_TYPE = 1; // Комплексное число: a+bi

    public const IMAGINARY_NUMBER_TYPE = 2; // Чисто мнимое число: 0+bi или bi

    public const REAL_NUMBER_TYPE = 3; // Вещественное число: a+0i или a

    public const ONLY_SUFFIX_TYPE = 4; // Найден только суффикс: i, можно разложить на 0+1i

    public const NUMBER_SPLIT_REGEXP =
        '` ^
            (                                   # Real part
                [-+]?(\d+\.?\d*|\d*\.?\d+)
                ([Ee][-+]?[0-2]?\d{1,3})?
            )
            (                                   # Imaginary part
                [-+]?(\d+\.?\d*|\d*\.?\d+)
                ([Ee][-+]?[0-2]?\d{1,3})?
            )?
            (                                   # Imaginary part is optional
                ([-+]?)
                ([ij]?)
            )
        $`uix';

    public const SUFFIX_SPLIT_REGEXP = '/^([\-+]?)([ij])$/ui';

    public static function defineType(string $complexStr): int
    {
        if (is_numeric($complexStr)) {
            return self::REAL_NUMBER_TYPE;
        }

        $validComplex = preg_match(self::NUMBER_SPLIT_REGEXP, $complexStr, $complexParts);
        if (!$validComplex) {
            // Здесь мнимой и вещественной части не найдено. Ищем валидный суффикс
            $validComplex = preg_match(self::SUFFIX_SPLIT_REGEXP, $complexStr, $complexParts);
            if (!$validComplex) {
                return self::INVALID_TYPE;
            }

            return self::ONLY_SUFFIX_TYPE;
        }

        if (('' === $complexParts[4]) && ('' !== $complexParts[9])) {
            // Здесь есть мнимая часть и есть суффикс
            return self::IMAGINARY_NUMBER_TYPE;
        }

        return self::COMPLEX_NUMBER_TYPE;
    }
}
