<?php declare(strict_types=1);

namespace App\Infrustructure\Service;

use App\Application\Exception\OperationException;
use App\Application\Exception\ValidationException;
use App\Application\Factory\ComplexNumberFactory;
use App\Application\Parser\ExpressionParser;
use App\Domain\Entity\ComplexNumber;
use App\Domain\Service\ComplexNumberServiceInterface;
use App\Domain\UseCase\Add;
use App\Domain\UseCase\Divide;
use App\Domain\UseCase\Multiply;
use App\Domain\UseCase\Subtract;

class ComplexNumberService implements ComplexNumberServiceInterface
{
    protected ComplexNumber $firstComplex;

    protected ComplexNumber $secondComplex;

    protected string $operation;

    /**
     * @throws ValidationException
     */
    public function __construct(string $expression)
    {
        $parser = new ExpressionParser();
        $factory = new ComplexNumberFactory();

        $firstComplex = $parser->getFirstComplex($expression);
        $secondComplex = $parser->getSecondComplex($expression);

        $this->firstComplex = $factory->create($firstComplex);
        $this->secondComplex = $factory->create($secondComplex);
        $this->operation = $parser->getOperation($expression);
    }

    /**
     * @throws OperationException
     */
    public function compute(): ComplexNumber
    {
        $operation = match ($this->operation) {
            '+' => new Add(),
            '-' => new Subtract(),
            '*' => new Multiply(),
            '/' => new Divide(),
            default => throw new OperationException('Невозможно определить оператор'),
        };

        return $operation([$this->firstComplex, $this->secondComplex]);
    }
}
