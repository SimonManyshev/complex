<?php declare(strict_types=1);

namespace App\Domain\Entity;

final readonly class ComplexNumber
{
    public function __construct(
        protected float $real = 0.0,
        protected float $imaginary = 0.0,
        protected string $suffix = 'i',
    ) {}

    public function __invoke(): string
    {
        $real = (string) $this->real;
        $imaginary = $this->imaginary > 0 ? '+'.$this->imaginary : (string) $this->imaginary;
        $suffix = $this->suffix;

        return '('.$real.$imaginary.$suffix.')';
    }

    public function getReal(): float
    {
        return $this->real;
    }

    public function getImaginary(): float
    {
        return $this->imaginary;
    }

    public function getSuffix(): string
    {
        return $this->suffix;
    }

    /**
     * @throws \JsonException
     */
    public function toJson(): false|string
    {
        $data = [
            'real' => $this->real,
            'imaginary' => $this->imaginary,
            'suffix' => $this->suffix,
        ];

        return json_encode($data, JSON_THROW_ON_ERROR);
    }
}
