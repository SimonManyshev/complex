<?php declare(strict_types=1);

namespace App\Domain\Service;

use App\Domain\Entity\ComplexNumber;

interface ComplexNumberServiceInterface
{
    public function compute(): ComplexNumber;
}
