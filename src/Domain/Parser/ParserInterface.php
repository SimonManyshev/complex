<?php declare(strict_types=1);

namespace App\Domain\Parser;

interface ParserInterface
{
    public function getFirstComplex(string $expression): string;

    public function getSecondComplex(string $expression): string;

    public function getOperation(string $expression): string;
}
