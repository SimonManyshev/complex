<?php declare(strict_types=1);

namespace App\Domain\UseCase;

use App\Application\Exception\OperationException;
use App\Domain\Entity\ComplexNumber;

interface OperationInterface
{
    /**
     * @param ComplexNumber[] $complexValues
     *
     * @throws OperationException
     */
    public function __invoke(array $complexValues): ComplexNumber;
}
