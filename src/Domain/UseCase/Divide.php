<?php declare(strict_types=1);

namespace App\Domain\UseCase;

use App\Application\Exception\OperationException;
use App\Domain\Entity\ComplexNumber;

class Divide implements OperationInterface
{
    /**
     * @param ComplexNumber[] $complexValues
     *
     * @throws OperationException
     */
    public function __invoke(array $complexValues): ComplexNumber
    {
        if (\count($complexValues) < 2) {
            throw new OperationException('Для вычисления обязательны минимум два числа');
        }

        $base = array_shift($complexValues);
        $result = clone $base;

        foreach ($complexValues as $complex) {
            if ($result->getSuffix() !== $complex->getSuffix()) {
                throw new OperationException('Суффиксы чисел не совпадают');
            }
            if (0.0 === $complex->getReal() && 0.0 === $complex->getImaginary()) {
                throw new OperationException('Деление на ноль');
            }

            $delta1 = ($result->getReal() * $complex->getReal()) +
                ($result->getImaginary() * $complex->getImaginary());
            $delta2 = ($result->getImaginary() * $complex->getReal()) -
                ($result->getReal() * $complex->getImaginary());
            $delta3 = ($complex->getReal() * $complex->getReal()) +
                ($complex->getImaginary() * $complex->getImaginary());

            $real = $delta1 / $delta3;
            $imaginary = $delta2 / $delta3;

            $result = new ComplexNumber(
                $real,
                $imaginary,
                (0.0 === $imaginary) ? null : max($result->getSuffix(), $complex->getSuffix())
            );
        }

        return $result;
    }
}
