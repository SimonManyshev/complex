<?php declare(strict_types=1);

namespace App\Domain\UseCase;

use App\Application\Exception\OperationException;
use App\Domain\Entity\ComplexNumber;

class Add implements OperationInterface
{
    /**
     * @param ComplexNumber[] $complexValues
     *
     * @throws OperationException
     */
    public function __invoke(array $complexValues): ComplexNumber
    {
        if (\count($complexValues) < 2) {
            throw new OperationException('Для вычисления обязательны минимум два числа');
        }

        $base = array_shift($complexValues);
        $result = clone $base;

        foreach ($complexValues as $complex) {
            $real = $result->getReal() + $complex->getReal();
            $imaginary = $result->getImaginary() + $complex->getImaginary();
            $result = new ComplexNumber(
                $real,
                $imaginary,
                (0.0 === $imaginary) ? null : max($result->getSuffix(), $complex->getSuffix())
            );
        }

        return $result;
    }
}
