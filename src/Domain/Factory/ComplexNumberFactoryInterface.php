<?php declare(strict_types=1);

namespace App\Domain\Factory;

use App\Domain\Entity\ComplexNumber;

interface ComplexNumberFactoryInterface
{
    public function create(string $str): ComplexNumber;
}
